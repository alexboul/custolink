# Custolink

Vous retrouverez dans ce fichier la documentation sur l'architecture du projet, les dépendances, et les instructions de déploiement.

## Architecture

Ce projet est composé de plusieurs services :
- Une base de données
- Une interface de gestion de base de donnée
- Une app web (front + back)


### Base de données

La base de données est en Postgres, et est obtenue à partir de l'image officielle de Postgres.
On peut s'y connecter avec un compte utilisateur :
```
username: user
password: password
```
On utilise le port **5432**.
Un volume est défini pour garder une persistance des données, et on retrouve également un data set (généré avec Mockaroo) qui peuple la base de données lors de son initialisation.

*Problème rencontré lors du développement : si l'on avait des volumes restant dûs à de précédents test, le conteneur considèrait que l'initialisation avait déjà été faite, et sautait cette étape, ce qui empêchait l'insertion de données.*


### Interface de gestion de base de données

Cette interface est générée via l'image Adminer.
On peut s'y connecter à cette adresse : `http://localhost:8080/` avec les données suivantes :
- Système : `PostgreSQL`
- Serveur : `db`
- Utilisateur : `user`
- Mot de passe : `password`
- Base de données : `custolink`


### Backend

La back de l'application est développé en Node.js avec le framework **Express**.

On y retrouve la configuration de la connexion à la base de données, ainsi qu'une simple fonction fait une query SQL et renvoie les données au front.

Ce back est mis en route via une image personnalisée que vous pourrez retrouver [ici sur Docker Hub](https://hub.docker.com/r/alexboule/custolink-backend). Vous pourrez également trouver le code source ainsi que le Dockerfile sur [mon repo Gitlab](). Vous trouverez des commentaire sur le rôle des lignes qui le composent.


### Frontend

Le front de l'application est réalisé avec Angular. Il est moche, on est d'accord, désolé pour les yeux.

Le site est accessible via l'adresse `http://localhost`

L'application n'est composé que d'une page, et fait une requête au back pour récupérer une liste d'étudiants.

Il s'agit la encore d'une image custom que vous pourrez retrouver sur [mon profil Docker Hub](https://hub.docker.com/r/alexboule/custolink-frontend) et plus en détail sur [mon repo Gitlab](). Vous trouverez des commentaire sur le rôle des lignes qui le composent.


## Dépendances

Au niveau architecture, Adminer et la Base de données se lancent en même temps, puis le back attend que la BDD soit lancée pour se mettre en route, et le front attends à son tour le back, demanière à arriver sur l'application en étant sûr que tout soit correctement lancé derrière.

Au niveau application, on utilise les packages Express, Postgres et Cors pour le back et Angular, Bootstrap et RxJS pour le front.


## Déploiement

Pour le déploiement de la stack, il suffit de se placer avec son terminal au niveau du fichier `docker-compose.yml` (en vérifiant de bien avoir dans le répertoire, en plus de ce fichier docker-compose.yml, un dossier `db` qui contient le fichier `students.sql`), et de lancer la commande `docker compose up -d`.

Cette commande va pull toutes les images nécessaires au fonctionnement de la stack, et mettre en route les conteneurs.

Vous pourrez ensuite accéder au site via l'adresse `http://localhost`, ainsi qu'a l'interface de gestion de BDD à l'adresse `http://localhost:8080`

