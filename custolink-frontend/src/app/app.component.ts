import { Component, OnInit } from '@angular/core';
import { StudentsService } from './students.service';
import { Student } from './student.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Custolink';
  students: Student[] = [];
  constructor(private studentsService: StudentsService) {}

  ngOnInit(): void {
    this.studentsService.getStudents().subscribe((data) => {
      this.students = data;
    });
  }
}
