create table students (
	id INT,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	email VARCHAR(50)
);
insert into students (id, first_name, last_name, email) values (1, 'Vernen', 'Kliche', 'vkliche0@bbb.org');
insert into students (id, first_name, last_name, email) values (2, 'Minna', 'Ferby', 'mferby1@google.com.au');
insert into students (id, first_name, last_name, email) values (3, 'Rosemary', 'Arrigo', 'rarrigo2@about.com');
insert into students (id, first_name, last_name, email) values (4, 'Melantha', 'Kilgannon', 'mkilgannon3@reuters.com');
insert into students (id, first_name, last_name, email) values (5, 'Bayard', 'Aberdeen', 'baberdeen4@ebay.com');
insert into students (id, first_name, last_name, email) values (6, 'Thornton', 'Hunnicot', 'thunnicot5@plala.or.jp');
insert into students (id, first_name, last_name, email) values (7, 'Fielding', 'Hehnke', 'fhehnke6@feedburner.com');
insert into students (id, first_name, last_name, email) values (8, 'Winston', 'Winsper', 'wwinsper7@bbb.org');
insert into students (id, first_name, last_name, email) values (9, 'Alfie', 'Gonzales', 'agonzales8@europa.eu');
insert into students (id, first_name, last_name, email) values (10, 'Bernadette', 'McGilvary', 'bmcgilvary9@posterous.com');
insert into students (id, first_name, last_name, email) values (11, 'Gilly', 'Takle', 'gtaklea@goodreads.com');
insert into students (id, first_name, last_name, email) values (12, 'Dominick', 'Roddick', 'droddickb@naver.com');
insert into students (id, first_name, last_name, email) values (13, 'Alvira', 'Hallows', 'ahallowsc@people.com.cn');
insert into students (id, first_name, last_name, email) values (14, 'Alfie', 'Earland', 'aearlandd@sina.com.cn');
insert into students (id, first_name, last_name, email) values (15, 'Kelwin', 'Huffey', 'khuffeye@youtube.com');
insert into students (id, first_name, last_name, email) values (16, 'Lev', 'Falk', 'lfalkf@timesonline.co.uk');
insert into students (id, first_name, last_name, email) values (17, 'Kordula', 'Ivantyev', 'kivantyevg@prlog.org');
insert into students (id, first_name, last_name, email) values (18, 'Inglebert', 'Alesin', 'ialesinh@last.fm');
insert into students (id, first_name, last_name, email) values (19, 'Kaila', 'Hazleton', 'khazletoni@g.co');
insert into students (id, first_name, last_name, email) values (20, 'Jaye', 'Hastilow', 'jhastilowj@noaa.gov');
