const Pool = require("pg").Pool;
const pool = new Pool({
  user: "user",
  host: "db",
  database: "custolink",
  password: "password",
  port: 5432,
});

const getStudents = (request, response) => {
  pool.query("SELECT * FROM students ORDER BY id ASC", (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

module.exports = {
  getStudents,
};
