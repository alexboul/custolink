const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;
const cors = require("cors");
const db = require("./queries");

app.use(cors());
app.use(bodyParser.json());
app.get("/students", db.getStudents);

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
